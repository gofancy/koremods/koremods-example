import codes.som.koffee.insns.jvm.`return`
import codes.som.koffee.types.void

transformers {
    method(
        "net.minecraft.world.level.block.CactusBlock",
        mapMethodName("m_7892_"), // entityInside
        constructMethodDescriptor(
            void,
            "net/minecraft/world/level/block/state/BlockState",
            "net/minecraft/world/level/Level",
            "net/minecraft/core/BlockPos",
            "net/minecraft/world/entity/Entity"
        ),
        ::transformEntityInside
    )
}

fun transformEntityInside(node: MethodNode) {
    logger.info("Transforming method CactusBlock#entityInside")
    
    node.insert {
        `return`
    }
}
